#!/usr/bin/env/python

import serial
import socket

REMOTE_IP = '127.0.0.1'
REMOTE_PORT = 10002
SERIAL_TTY = '/dev/ttyACM0'
SERIAL_BAUD = 9600

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((REMOTE_IP, REMOTE_PORT))

buddy = serial.Serial(SERIAL_TTY, SERIAL_BAUD)

while True:
	s.send(serial.read())

s.close()
